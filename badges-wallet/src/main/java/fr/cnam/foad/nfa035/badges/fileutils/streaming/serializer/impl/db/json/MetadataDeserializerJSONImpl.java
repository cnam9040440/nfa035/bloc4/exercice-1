package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Set;
import java.util.stream.Collectors;

public class MetadataDeserializerJSONImpl extends MetadataDeserializerDatabaseImpl {

    private static final Logger LOG = LogManager.getLogger(JSONWalletDeserializerDAImpl.class);

    /**
     * {@inheritDoc}
      * @param media
     * @return
     * @throws IOException
     */
    @Override
    public Set<DigitalBadge> deserialize(WalletFrameMedia media) throws IOException {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        BufferedReader br = media.getEncodedImageReader(false);
        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);
        return br.lines()
                .map(
                        l-> {
                            try {
                                String badge = l.split(",\\{\"payload")[0].split(".*badge\":")[1];
                                DigitalBadge digitalBadge = objectMapper.readValue(badge, DigitalBadge.class);
                                return digitalBadge.getMetadata().getImageSize() == -1 ? null : digitalBadge;
                            } catch (IOException ioException) {
                                LOG.error("Problème de parsage JSON, on considère l'enregistrement Nul", ioException);
                            }

                            return null;
                        }
                ).filter(x -> x!=null).collect(Collectors.toSet());
    }


    /**
     * Peremet de lire la dernière ligne d'un fichier en accès direct
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for(long seek = length; seek >= 0; --seek){
            file.seek(seek);
            char c = (char)file.read();
            if(c != '\n' || seek == 1)
            {
                builder.append(c);
            }
            else{
                builder = builder.reverse();
                break;
            }
        }
        return builder.toString();
    }
}
