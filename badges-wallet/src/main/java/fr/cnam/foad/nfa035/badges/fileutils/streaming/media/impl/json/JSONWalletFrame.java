package fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

/**
 * Implémentation d'ImageFrame pour un Fichier json comme canal
 */
public class JSONWalletFrame extends WalletFrame {

    private static final Logger LOG = LogManager.getLogger(JSONWalletFrame.class);

    FileOutputStream encodedImageOutput;

    private long numberOfLines;

    public JSONWalletFrame(RandomAccessFile walletDatabase, FileOutputStream encodedImageOutput) {
        super(walletDatabase);
        this.encodedImageOutput = encodedImageOutput;
    }

    /**
     * {@inheritDoc}
      * @return FileOutputStream
     */
    @Override
    public FileOutputStream getEncodedImageOutput() throws IOException {

        if (encodedImageOutput == null){

            RandomAccessFile file = getChannel();
            this.encodedImageOutput = new FileOutputStream(file.getFD());
            long fileLength = file.length();

            JsonFactory jsonFactory = new JsonFactory();
            jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
            ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

            if(fileLength == 0) {
                // Ecriture de l'amorce
                Writer writer = new PrintWriter(encodedImageOutput, true, StandardCharsets.UTF_8);
                writer.write("[[{badge:{},{payload:}],)");
                this.numberOfLines = 0;
                writer.flush();
            }
            else{
                BufferedReader br = getEncodedImageReader(false);
                String lastLine = MetadataDeserializerJSONImpl.readLastLine(file);
                LOG.debug("Dernière ligne du fichier : {}", lastLine);
                String stringNumberOfLines = lastLine.split(",\\{\"payload")[0].split(".badgeId\":")[1];

                this.numberOfLines = Long.parseLong(stringNumberOfLines) - 1;
                file.seek(fileLength);
            }

        }
        return encodedImageOutput;

    }
}
