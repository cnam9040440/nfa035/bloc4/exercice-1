package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletDeserializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletDeserializerDAImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletSerializerDAImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Set;

public class JSONBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(JSONBadgeWalletDAOImpl.class);

    private File walletDatabase;

    public JSONBadgeWalletDAOImpl(String dbPath) throws IOException{
        this.walletDatabase = new File(dbPath);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    @Override
    public void addBadge(File image) throws IOException {
        LOG.error("Non supporté");
        throw new IOException("Méthode non supportée, utilisez plutôt addBadge(DigitalBadge badge)");
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    @Override
    public void getBadge(OutputStream imageStream) throws IOException {
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        }
    }

    /**
     * Permet de récupérer les metadonnées du Wallet
     *
     * @return List<DigitalBadgeMetadata>
     * @throws IOException
     */
    @Override
    public Set<DigitalBadge> getWalletMetadata() throws IOException {
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            return new MetadataDeserializerJSONImpl().deserialize(media);
        }
    }

   /** private Set<DigitalBadge> getWalletMetadata() throws IOException {
        return null;
    }**/

    /**
     * Permet de récupérer un badge du Wallet à partir de ses métadonnées
     *
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException {
        Set<DigitalBadge> metas = this.getWalletMetadata();
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            new JSONWalletDeserializerDAImpl(metas, imageStream).deserialize(media, meta);
        }
    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void addBadge(DigitalBadge badge) throws IOException {
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))){
            ImageStreamingSerializer serializer = new JSONWalletSerializerDAImpl(getWalletMetadata());
            serializer.serialize(badge, media);
        }
    }
}
