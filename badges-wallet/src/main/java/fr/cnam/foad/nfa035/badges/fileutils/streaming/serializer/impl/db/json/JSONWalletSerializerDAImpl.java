package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.proxy.Base64OutputStreamProxy;
import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Set;

public class JSONWalletSerializerDAImpl extends AbstractStreamingImageSerializer<DigitalBadge, JSONWalletFrame> {

    Set<DigitalBadge> metas;

    public JSONWalletSerializerDAImpl(Set<DigitalBadge> metas) {
        this.metas = metas;
    }


    /**
     * {@inheritDoc}
      * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public void serialize(DigitalBadge source, JSONWalletFrame media) throws IOException {
        RandomAccessFile random = media.getChannel();
        if (metas.contains(source)){
            throw new IOException("Badge déjà présent dans le Wallet");
        }
        long size = Files.size(source.getBadge().toPath());
        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

      //  DigitalBadgeMetadata digitalBadgeMetadata = new DigitalBadgeMetadata((int)media.getNumberOfLines()+1, 0, size);
        DigitalBadge badge = new DigitalBadge(new DigitalBadgeMetadata((int)media.getNumberOfLines()+1, 0, size), source.getSerial(), source.getBegin(), source.getEnd());

        try(OutputStream os = media.getEncodedImageOutput()) {
            objectMapper.writeValue(os,badge);
            try(OutputStream eos = getSerializingStream(media)) {
                objectMapper.writeValue(eos, getSourceInputStream(source));

            }
        }




    }

    /**
     * Utile pour récupérer un Flux de lecture de la source à sérialiser
     *
     * @param source
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getSourceInputStream(DigitalBadge source) throws IOException {
        return new FileInputStream(source.getBadge());
    }

    /**
     * Permet de récupérer le flux d'écriture et de sérialisation vers le media
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(JSONWalletFrame media) throws IOException {
        return new Base64OutputStreamProxy(new Base64OutputStream(media.getEncodedImageOutput(),true,0,null));
    }
}
